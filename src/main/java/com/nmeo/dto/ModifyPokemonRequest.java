package com.nmeo.dto;

import com.nmeo.models.PokemonPower;
import com.nmeo.models.PokemonType;

import javax.lang.model.util.Elements;
import java.util.List;

public class ModifyPokemonRequest {
    public String pokemonName;
    public PokemonType type;
    public Integer lifePoints;
    public List<PokemonPower> powers;
}

