package com.nmeo.dto;

import com.nmeo.models.Pokemon;

import java.util.List;

public class ResultResponse {
    public List<Pokemon> result;

    public ResultResponse(List<Pokemon> p) {
        this.result = p;
    }
}
