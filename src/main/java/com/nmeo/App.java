package com.nmeo;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.dto.ResultResponse;
import com.nmeo.models.Pokemon;
import com.nmeo.models.PokemonType;
import io.javalin.Javalin;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.*;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());
    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;


        Set<Pokemon> pokedex = new HashSet<Pokemon>();

        Javalin.create()
        .get("/api/status", ctx -> {
            logger.debug("Status handler triggered", ctx);
            ctx.status(200);
        })
        .post("/api/create", ctx -> {
            Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
            if(!pokedex.contains(pokemon)){
                pokedex.add(pokemon);
                ctx.status(200);

            }
            else{
                ctx.status(400);
            }

        })
        .get("/api/searchByName", ctx -> {
            String nameToSearch = ctx.queryParam("name");
            // Convert the input name to uppercase for case-insensitive comparison
            String upperCaseName = nameToSearch.toUpperCase();

            // Check if the uppercase name is a valid PokemonType
            boolean isValidType = false;
            try {
                PokemonType.valueOf(upperCaseName);
                isValidType = true;
            } catch (IllegalArgumentException e) {
                // Enum value not found, isValidType remains false
            }

            if(!isValidType){
                ctx.status(400);
            }


            List<Pokemon> pokemonList = new ArrayList<>();

            for (Pokemon pokemon : pokedex) {
                if (pokemon.getPokemonName().toUpperCase().contains(upperCaseName)) {
                    pokemonList.add(pokemon);
                }
            }

            if (!pokemonList.isEmpty()) {
                ctx.json(new ResultResponse(pokemonList));
                ctx.status(200);
            } else if (pokemonList.isEmpty()){
                ctx.json(new ResultResponse(pokemonList));
                ctx.status(200);
            }else{
                ctx.status(400);
            }
        })
                .post("/api/modify", ctx -> {
                    ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);

                    if (request.pokemonName.isEmpty()) {
                        ctx.status(400); // Bad Request - Empty Pokemon name
                        return;
                    }

                    Pokemon pokemonToModify = null;
                    for (Pokemon p : pokedex) {
                        if (p.getPokemonName().equalsIgnoreCase(request.pokemonName)) {
                            pokemonToModify = p;
                            break;
                        }
                    }

                    if (pokemonToModify == null) {
                        ctx.status(404); // Not Found - Pokemon not in pokedex
                        return;
                    }

                    if (request.type != null) {
                        pokemonToModify.setType(request.type);
                    }

                    if (request.lifePoints != null) {
                        pokemonToModify.setLifePoints(request.lifePoints );
                    }

                    if (request.powers != null) {
                        pokemonToModify.addPowers(request.powers);
                    }

                    ctx.status(200); // OK - Pokemon modified successfully
                })


                .get("/api/searchByType", ctx -> {
            String typeToSearch = ctx.queryParam("type");

            // Check if the uppercase name is a valid PokemonType
            boolean isValidType = false;
            try {
                PokemonType.valueOf(typeToSearch);
                isValidType = true;
            } catch (IllegalArgumentException e) {
                // Enum value not found, isValidType remains false
            }

            if(!isValidType){
                ctx.status(400);
            }

            List<Pokemon> pokemonLists = new ArrayList<Pokemon>();

            for (Pokemon pokemon : pokedex) {
                if (pokemon.getType().toString().equals(typeToSearch)) {
                    pokemonLists.add(pokemon);
                }
            }

            // Now you can respond with the list of found Pokemon, e.g., as JSON
            ctx.json(new ResultResponse(pokemonLists));
        })

        .start(port);
    }
}