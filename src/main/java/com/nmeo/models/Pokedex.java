package com.nmeo.models;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;

public class Pokedex implements Iterable<Pokemon> {

    public Set<Pokemon> pokemons = new HashSet<>();

    @NotNull
    @Override
    public Iterator<Pokemon> iterator() {
        return null;
    }

    @Override
    public void forEach(Consumer<? super Pokemon> action) {
        Iterable.super.forEach(action);
    }
}
