package com.nmeo.models;

import java.util.ArrayList;
import java.util.Objects;
import java.util.List;

public class Pokemon {
    private String pokemonName;
    private PokemonType type;
    private int lifePoints;
    private List<PokemonPower> powers;

    // Constructor

    // Constructor with mandatory fields

    public Pokemon(){

    }
    public Pokemon(String pokemonName) {
        this.pokemonName = pokemonName;
        this.type = PokemonType.ELECTRIC;
        // Default values for optional fields
        this.lifePoints = 100;
        this.powers = null;
    }

    // Constructor with all fields
    public Pokemon(String pokemonName, PokemonType element, int lifePoints, List<PokemonPower> powers) {
        this.pokemonName = pokemonName;
        this.type = element;
        this.lifePoints = lifePoints;
        this.powers = powers;
    }

    // Getters and Setters
    public String getPokemonName() {
        return pokemonName;
    }

    public void setPokemonName(String pokemonName) {
        this.pokemonName = pokemonName;
    }

    public PokemonType getType() {
        return type;
    }

    public void setType(PokemonType type) {
        this.type = type;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public List<PokemonPower> getPowers() {
        return powers;
    }

    public void setPowers(List<PokemonPower> powers) {
        this.powers = powers;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Pokemon pokemon = (Pokemon) obj;
        return Objects.equals(pokemonName, pokemon.pokemonName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pokemonName);
    }

    public void addPowers(List<PokemonPower> p){
        this.powers.addAll(p);
    }
}
